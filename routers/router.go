package routers

import (
	"assignment3/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})

	//LoginController
	beego.Router("/login", &controllers.LoginController{})

	beego.Router("/register", &controllers.RegisterController{})

	beego.Router("/logout", &controllers.LogoutController{})

	beego.Router("/secrets", &controllers.SecretsController{})
	beego.Router("/secrets/add", &controllers.SecretsAddController{})
	beego.Router("/secrets/remove", &controllers.SecretsRemoveController{})

	beego.Router("/profile", &controllers.ProfileController{})

	beego.Router("/reset", &controllers.ResetController{})

}
