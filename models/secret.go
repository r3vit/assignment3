package models

//A Secret represents a couple key-variable.
type Secret struct {
	Key   string
	Value string
}
