package models

//A User represents an user registered to the webapp.
type User struct {
	Name      string
	Password  string
	LastLogin string
	Avatar    string
	Info      string
	Secrets   []Secret
}
