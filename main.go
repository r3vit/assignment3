package main

import (
	_ "assignment3/routers"

	"github.com/astaxie/beego"
)

func main() {
	//level error log
	beego.SetLevel(beego.LevelError)

	//run
	beego.Run()
}
