<img src="https://i.imgur.com/4KXWWBN.png" alt="Logo" width="200" height="200">

# Assignment3

A simple `secrets` management tool.

## Features

* Written in uncomplicated Go (Golang)
* Easy to use
* Works on Mac, Linux and (maybe) Windows (Dockerfile! 🐳)
* Blazing fast ⚡️

## Installation (simple): from source for Everyone! with Docker 🐳

Make sure that Docker is installed! (https://golang.org/doc/install#install)

```bash
$ git clone git@gitlab.com:r3vit/assignment3.git
$ cd assignment3/
$ docker build -t r3vit/assignment3 .
$ docker run -it --rm --name ma-instance -p 8081:8080 -v $PWD:/go/src/assignment3 -w /go/src/assignment3 r3vit/assignment3
```

Now open your browser and navigate to http://localhost:8081

## Installation (advanced): from source for devs

Make sure that Go is installed! (https://golang.org/doc/install#install)

(and Glide https://glide.sh)

```bash
$ git clone git@gitlab.com:r3vit/assignment3.git
$ cd assignment3/
$ glide up
$ go get github.com/beego/bee
$ bee run
```

## Usage

### Basic usage

1. connect to localhost:8081
2. login with default credentials: admin - admin (or register)
3. add a new secret to your list!

## Things done/todo

* [x] A proper framework (Beego https://github.com/beego)
* [x] Login with ~~cookies~~ sessions
* [x] MVC pattern with Front Controller
* [x] Bolt key-value database
* [x] ~~Basic GUI~~ 🎀 GUI 🎀
* [x] User registration 🙋‍
* [x] Users list for admin view
* [ ] Complete encryption of the secrets
* [ ] Add files encryption
