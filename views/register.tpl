

<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Register - Secrets</title>
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/reset.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/site.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/container.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/grid.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/header.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/image.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/menu.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/divider.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/segment.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/input.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/button.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/list.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/message.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/icon.css">

  <script src="{{urlfor "MainControler"}}/static/js/jquery-3.2.1.min.js"></script>
  <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.js"></script>
  <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/transition.js"></script>

  <style type="text/css">
    body {
      background-color: #FFF;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <img src="{{urlfor "MainControler"}}/static/img/logo.png" class="image">
      <div class="content">
        Register a new account
      </div>
    </h2>
    <form class="ui large form" method="post" action="/register">
      <div class="ui segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="username" placeholder="Your username">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="text" name="password" placeholder="Your password">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="image icon"></i>
            <input type="avatar" name="avatar" placeholder="Avatar URL">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="info icon"></i>
            <input type="info" name="info" placeholder="Info about you">
          </div>
        </div>
        <div><button type="submit" class="ui fluid large teal submit button">Register</button></div>
      </div>
    </form>
  </div>
</div>

  <script src="/{{urlfor "MainControler"}}/static/js/reload.min.js"></script>
</body>

</html>
