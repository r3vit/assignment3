

<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Login - Secrets</title>
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/reset.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/site.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/container.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/grid.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/header.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/image.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/menu.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/divider.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/segment.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/input.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/button.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/list.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/message.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/icon.css">

  <script src="{{urlfor "MainControler"}}/static/js/jquery-3.2.1.min.js"></script>
  <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.js"></script>
  <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/transition.js"></script>

  <style type="text/css">
    body {
      background-color: #FFF;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <img src="{{urlfor "MainControler"}}/static/img/logo.png" class="image">
      <div class="content">
        Log-in to your account
      </div>
    </h2>

    <form class="ui large form" method="post" action="/login">
      <div class="ui segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="username" placeholder="Name: admin">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password: admin">
          </div>
        </div>
        <div><button type="submit" class="ui fluid large teal submit button">Login</button></div>
      </div>
    </form>


<form class="ui medium form" method="get" action="/register">
  <div class="ui segment">
  Not registered?
  <a href="{{urlfor "RegisterController"}}">
      <div><button type="submit" class="ui fluid medium teal submit button">Register</button></div>
  </a>
</div>

    </form>

    <div class="ui message">


      <div> <h2> Reset</h2></div>
      <div><a href="/reset">/reset</a></div>

    </div>
  </div>
</div>

  <script src="/{{urlfor "MainControler"}}/static/js/reload.min.js"></script>
</body>

</html>
