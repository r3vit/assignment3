<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>{{.Name}} - Profile Edit</title>
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/reset.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/site.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/container.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/grid.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/header.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/image.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/menu.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/divider.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/segment.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/input.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/button.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/list.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/message.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/icon.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/card.css">



    <script src="{{urlfor "MainControler"}}/static/js/jquery-3.2.1.min.js"></script>
    <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.js"></script>
    <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/transition.js"></script>

    <style type="text/css">
      body {
        background-color: #FFF;
      }
      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }
      .column {
        max-width: 450px;
      }
      h2 {
  margin: 2em 0em;
}
.ui.container {
  padding-top: 5em;
  padding-bottom: 5em;
}
.ui.card > .image {
  background: rgba(0, 0, 0, 0);
}
    </style>
  </head>
  <body>

<div class="ui container">
  <div class="ui grid">
    <div class="ten wide column centered">
      <h2 class="ui teal image header">
        <div class="content">
          Edit "{{.Uname}}" Profile
        </div>
      </h2>
      <form class="ui large form" method="post" action="/profile">
        <div class="ui segment">
          <div class="field">
            <div class="ui left icon input">
              <i class="lock icon"></i>
              <input type="text" name="password" placeholder="Your Password">
            </div>
          </div>
          <div class="field">
            <div class="ui left icon input">
              <i class="image icon"></i>
              <input type="avatar" name="avatar" value="{{.Uavatar}}">
            </div>
          </div>
          <div class="field">
            <div class="ui left icon input">
              <i class="info icon"></i>
              <input type="info" name="info"  value="{{.Uinfo}}">
            </div>
          </div>
          <div class="field">
            <div class="ui buttons right floated">
              <a href="/secrets"><i class="ui button">Cancel</i></a>
              <div class="or"></div>
              <button type="submit" class="ui teal button active">Save</button>
            </div>
          </div>
          <div class="field">
          </div>

        </div>
      </form>

    </div>
  </div>
</div>


</div>

</body>
</html>
