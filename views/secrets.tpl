<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>{{.Name}} - Secrets</title>
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/reset.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/site.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/container.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/grid.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/header.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/image.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/menu.css">

  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/divider.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/segment.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/input.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/button.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/list.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/message.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/icon.css">
  <link rel="stylesheet" type="text/css" href="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/card.css">



    <script src="{{urlfor "MainControler"}}/static/js/jquery-3.2.1.min.js"></script>
    <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/form.js"></script>
    <script src="{{urlfor "MainControler"}}/static/css/Semantic-UI-CSS/components/transition.js"></script>

    <style type="text/css">
      body {
        background-color: #FFF;
      }
      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }
      .column {
        max-width: 450px;
      }
      h2 {
  margin: 2em 0em;
}
.ui.container {
  padding-top: 5em;
  padding-bottom: 5em;
}
.ui.card > .image {
  background: rgba(0, 0, 0, 0);
}
    </style>
  </head>
  <body>

<div class="ui container">
<div class="ui grid">
  <div class="five wide column">

    <div class="ui card">
      <div class="image">
        <img src=" {{.Uavatar}}">
      </div>
      <div class="content">
        <a class="header">{{.Uname}}</a>
        <div class="meta">
          <p class="date">Last accesss {{.Ulastlogin}}</p>
        </div>
        <div class="description">
          {{.Uinfo}}
        </div>
      </div>
      <div class="extra content">
          <i class="lock icon"></i>
          {{$length := len .Usecrets}} {{$length}} Secrets stored
      </div>
      <div class="extra content">
        <span class="right floated">
        <form method="get" action="/logout">
        <button class="ui negative basic button" type="submit">Logout</button>
        </form>
      </span>
      <span class="right floated">
        <form method="get" action="/profile">
        <button class="ui teal basic button" type="submit">Edit</button>
        </form>
      </span>
      </div>
    </div>

  </div>
  <div class="ten wide column">

      <div>
        {{range .Usecrets}}
        <div class="ui segment ">

          <form action="/secrets/remove" method="post" class="reset">
            Secret key is "{{.Key}}" and value is "{{.Value}}"
            <input type="hidden" name="key" value="{{.Key}}">
          <a href="javascript:;" onclick="parentNode.submit();">
          <button class="compact ui" style="float: right;">
             <i class="right floated trash red icon"></i>
           </button>
           </a>
         </form>

        </div>

      {{end}}
      </div>
</div>

</div>

<div class="ui grid">
  <div class="five wide column">

      <h1>Add secret </h1>
      <form class="ui form" method="post" action="/secrets/add">
        <div class="field">
          <label>Key</label>
          <input type="text" name="key" placeholder="key">
        </div>
        <div class="field">
          <label>Value</label>
          <input type="text" name="value" placeholder="value">
        </div>

        <button class="ui button" type="submit">Add</button>
      </form>

  </div>

{{if .Uadmin}}
  <div class="five wide column">
    <h1>Users list </h1>
  {{range .Auserlist}}
        <div class="ui segment ">

           <span>"{{.}}"</span>
         </div>
  {{end}}

{{end}}

</div>


</div>

</body>
</html>
