# Base image is in https://registry.hub.docker.com/_/golang/
# Refer to https://blog.golang.org/docker for usage
FROM golang:1.9
MAINTAINER Marco Capobussi macatmail@gmail.com

# Install beego and the bee tool
RUN go get github.com/astaxie/beego && go get github.com/beego/bee

# Expose the application on port 8081
EXPOSE 8080

# Set the entry point of the container to the bee command
CMD ["bee", "run"]
