package controllers

import (
	"assignment3/models"
	"fmt"
	"log"
	"strings"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//SecretsController is the secret management controller of the webapp.
//It manages the secrets list page.
type SecretsController struct {
	beego.Controller
}

func (this *SecretsController) Get() {

	session := this.StartSession()

	//ckeck if user is logged
	if session.Get("uname") == nil {
		this.TplName = "secrets.unathorized.tpl"
		return
	}

	//load the bolt k-v db
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//get session data and set user data variables
	userData := models.User{
		Name:      session.Get("uname").(string),
		Password:  session.Get("upassword").(string),
		LastLogin: session.Get("ulastlogin").(string),
		Avatar:    session.Get("uavatar").(string),
		Info:      session.Get("uinfo").(string),
	}

	//read secrets for current username
	secrets := []models.Secret{}

	//create if not exists Bucket
	err = db.Update(func(tx *bolt.Tx) error {
		//create the bucket for admin
		_, err := tx.CreateBucket([]byte(userData.Name))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})

	//select all the secrets for current user
	db.View(func(tx *bolt.Tx) error {
		// Now bucket exists and has keys
		b := tx.Bucket([]byte(userData.Name))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {

			val, err := decrypt([]byte(v), []byte(userData.Password))
			if err != nil {
				fmt.Println(err.Error())
			}

			secrets = append(secrets, models.Secret{
				Key:   string(k),
				Value: string(val)})
		}

		return nil
	})

	//populate userData.Secrets model
	userData.Secrets = secrets

	//populate this data and pass to the view
	this.Data["Uname"] = userData.Name
	this.Data["Upassword"] = userData.Password
	this.Data["Uadmin"] = false
	this.Data["Uavatar"] = userData.Avatar
	this.Data["Ulastlogin"] = userData.LastLogin
	this.Data["Uinfo"] = userData.Info
	this.Data["Usecrets"] = userData.Secrets

	if session.Get("uadmin").(string) == "true" {
		this.Data["Uadmin"] = true
		listUsers := []string{}

		//select all the secrets for current user
		db.View(func(tx *bolt.Tx) error {
			// Now bucket exists and has keys
			b := tx.Bucket([]byte("users"))
			c := b.Cursor()
			for k, v := c.First(); k != nil; k, v = c.Next() {

				if strings.Contains(string(k), ".Name") {
					listUsers = append(listUsers, string(v))
				}
			}

			return nil
		})

		this.Data["Auserlist"] = listUsers
	}

	this.TplName = "secrets.tpl"
}
