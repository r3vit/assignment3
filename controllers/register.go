package controllers

import (
	"assignment3/models"
	"errors"
	"fmt"
	"log"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//RegisterController is the Register controller of the webapp.
//As .GET will provide the register page.
//As .POST will register the user info in db
type RegisterController struct {
	beego.Controller
}

func (this *RegisterController) Get() {
	session := this.StartSession()

	//ckeck if user is already logged
	if session.Get("uname") != nil {
		this.Redirect("/secrets", 302)
		return
	}

	this.TplName = "register.tpl"
}

func (this *RegisterController) Post() {
	session := this.StartSession()

	//ckeck if user is already logged
	if session.Get("uname") != nil {
		this.Redirect("/secrets", 302)
		return
	}
	//check if username is "admin"
	if this.GetString("username") == "admin" {
		this.Data["Error"] = "Name: \"Admin\" not permitted"
		this.TplName = "register.error.tpl"
		return
	}
	//check if a field is empty
	if this.GetString("username") == "" || this.GetString("password") == "" || this.GetString("avatar") == "" || this.GetString("info") == "" {
		this.Data["Error"] = "One field is empty!"
		this.TplName = "register.error.tpl"
		return
	}

	//save new user data
	//load the bolt k-v db
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	passwordHash, _ := hashPassword(this.GetString("password"))

	//set username Data Model
	userData := models.User{
		Name:      this.GetString("username"),
		Password:  passwordHash,
		LastLogin: "Never",
		Avatar:    this.GetString("avatar"),
		Info:      this.GetString("info"),
	}

	//Insert new data in db
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		if b.Get([]byte(userData.Name+".Name")) != nil {
			return errors.New("Name already taken!")
		}

		err = b.Put([]byte(userData.Name+".Name"), []byte(userData.Name))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Password"), []byte(userData.Password))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Avatar"), []byte(userData.Avatar))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Info"), []byte(userData.Info))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		return err
	})

	if err != nil {
		this.Data["Error"] = err.Error()
		this.TplName = "register.error.tpl"
		return
	}

	this.TplName = "register.successfull.tpl"
}
