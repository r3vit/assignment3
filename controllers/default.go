package controllers

import (
	"github.com/astaxie/beego"
)

//MainController is the default controller of the webapp
type MainController struct {
	beego.Controller
}

func (this *MainController) Get() {
	this.TplName = "index.tpl"
}
