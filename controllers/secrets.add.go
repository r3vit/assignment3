package controllers

import (
	"assignment3/models"
	"fmt"
	"log"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//SecretsAddController is the secrets adder controller of the webapp.
//It adds a secret for current user.
type SecretsAddController struct {
	beego.Controller
}

func (this *SecretsAddController) Post() {
	session := this.StartSession()

	//check if user is logged
	if session.Get("uname") == nil {
		this.TplName = "secrets.unathorized.tpl"
		return
	}

	//retrieve post data for secrets
	key := this.GetString("key")
	value := this.GetString("value")

	val, _ := encrypt([]byte(value), []byte(session.Get("upassword").(string)))

	//load the bolt k-v db
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//set username
	userData := models.User{
		Name: session.Get("uname").(string),
	}

	//insert the secret in the user bucket
	db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(userData.Name))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		b = tx.Bucket([]byte(userData.Name))
		err = b.Put([]byte(key), []byte(val))
		return err
	})

	this.Redirect("/secrets", 302)
	return
}
