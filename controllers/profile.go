package controllers

import (
	"assignment3/models"
	"fmt"
	"log"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//ProfileController is the profile controller of the webapp.
//As .GET will provide the profile page.
//As .POST will edit the info in db and session
type ProfileController struct {
	beego.Controller
}

func (this *ProfileController) Get() {
	session := this.StartSession()

	//ckeck if user is logged
	if session.Get("uname") == nil {
		this.TplName = "secrets.unathorized.tpl"
		return
	}

	//set user data variables
	userData := models.User{
		Name:      session.Get("uname").(string),
		Password:  session.Get("upassword").(string),
		LastLogin: session.Get("ulastlogin").(string),
		Avatar:    session.Get("uavatar").(string),
		Info:      session.Get("uinfo").(string),
	}

	//populate this data and pass to the view
	this.Data["Uname"] = userData.Name
	this.Data["Upassword"] = userData.Password
	this.Data["Uavatar"] = userData.Avatar
	this.Data["Ulastlogin"] = userData.LastLogin
	this.Data["Uinfo"] = userData.Info

	this.TplName = "profile.tpl"

}

func (this *ProfileController) Post() {
	session := this.StartSession()

	//ckeck if user is logged
	if session.Get("uname") == nil {
		this.TplName = "secrets.unathorized.tpl"
		return
	}

	//check if one field is empty
	if this.GetString("password") == "" || this.GetString("avatar") == "" || this.GetString("info") == "" {
		this.Data["Error"] = "One field is empty!"
		this.TplName = "register.error.tpl"
		return
	}

	passwordHash, _ := hashPassword(this.GetString("password"))

	//set  Data Model
	userData := models.User{
		Name:     session.Get("uname").(string),
		Password: passwordHash,
		Avatar:   this.GetString("avatar"),
		Info:     this.GetString("info"),
	}

	//save new user data
	//load the bolt k-v db
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//update te database infos
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		err = b.Put([]byte(userData.Name+".Password"), []byte(userData.Password))
		if err != nil {
			return fmt.Errorf("Profile edit bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Avatar"), []byte(userData.Avatar))
		if err != nil {
			return fmt.Errorf("Profile edit bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Info"), []byte(userData.Info))
		if err != nil {
			return fmt.Errorf("Profile edit bolt: %s", err)
		}

		return nil
	})

	//set session with new data
	session.Set("upassword", string(passwordHash))
	session.Set("uavatar", userData.Avatar)
	session.Set("uinfo", userData.Info)

	this.Redirect("/secrets", 302)

}
