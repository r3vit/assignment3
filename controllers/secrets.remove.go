package controllers

import (
	"assignment3/models"
	"log"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//SecretsRemoveController is the secret remover controller of the webapp.
//It removes the  passed secret from current user.
type SecretsRemoveController struct {
	beego.Controller
}

func (this *SecretsRemoveController) Post() {

	session := this.StartSession()

	//check if user is logged
	if session.Get("uname") == nil {
		this.TplName = "secrets.unathorized.tpl"
		return
	}

	//retrieve post data for secrets
	key := this.GetString("key")

	//load the bolt k-v db
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//set username
	userData := models.User{
		Name: session.Get("uname").(string),
	}

	//delete the secret
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(userData.Name))
		err := b.Delete([]byte(key))
		return err
	})

	this.Redirect("/secrets", 302)
	return
}
