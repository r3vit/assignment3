package controllers

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//LoginController is the login controller of the webapp.
//It reads the input login form and generate the correct session variables.
type LoginController struct {
	beego.Controller
}

func (this *LoginController) Post() {
	//get the input form data
	username := this.GetString("username")
	password := this.GetString("password")

	//load the bolt k-v db
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//check if user exists ("users".{{username}}.Name)
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		if b.Get([]byte(username+".Name")) == nil {
			return errors.New("User does not exists!")
		}
		return nil
	})
	if err != nil {
		//if an error occurs, show login.error template
		this.Data["Error"] = err.Error()
		this.TplName = "login.error.tpl"
		return
	}

	//if the user exists,
	//check if password is correct
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		if b.Get([]byte(username+".Password")) != nil {
			//if user exists
			hashedPassword := b.Get([]byte(username + ".Password"))
			if comparePassword(password, string(hashedPassword)) {
				//if password is correct
				//Set the session for successful login
				session := this.StartSession()

				ulastlogin := time.Now()

				session.Set("uname", string(username))
				session.Set("upassword", string(hashedPassword))
				session.Set("ulastlogin", ulastlogin.String()[:19])
				session.Set("uavatar", string(b.Get([]byte(username+".Avatar"))))
				session.Set("uinfo", string(b.Get([]byte(username+".Info"))))
				session.Set("uadmin", "false")

				//check if admin is the user and set sessions
				if username == "admin" {
					session.Set("uadmin", "true")
				}

				this.Redirect("/secrets", 302)
			} else {
				return errors.New("Password is wrong!")
			}
		}

		return nil
	})
	if err != nil {
		this.Data["Error"] = err.Error()
		this.TplName = "login.error.tpl"
		return
	}

	this.Redirect("/secrets", 302)

}

func (this *LoginController) Get() {
	this.TplName = "index.tpl"
}
