package controllers

import (
	"github.com/astaxie/beego"
)

//LogoutController is the logout controller of the webapp.
//It destroy all the sessions.
type LogoutController struct {
	beego.Controller
}

func (this *LogoutController) Get() {
	//destroy the session
	this.DestroySession()

	this.TplName = "logout.tpl"
}
