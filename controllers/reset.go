package controllers

import (
	"assignment3/models"
	"fmt"
	"log"
	"os"

	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

//ResetController is the reset controller of the webapp.
//It will destroy the session, delete the db file, recreate the db with admin user.
type ResetController struct {
	beego.Controller
}

func (this *ResetController) Get() {
	this.DestroySession()

	databasePath := "bolt.db"

	//delete bolt db if exists
	if _, err := os.Stat(databasePath); err == nil {
		err = os.Remove(databasePath)
		if err != nil {
			log.Fatal(err)
		}
	}

	//recreate the bolt k-v db
	db, err := bolt.Open(databasePath, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//set admin user data
	passwordHash, _ := hashPassword("admin")
	userData := models.User{
		Name:      "admin",
		Password:  passwordHash,
		LastLogin: "Never",
		Avatar:    "http://localhost:8080/static/img/logo.png",
		Info:      "I'm the admin user.",
	}

	err = db.Update(func(tx *bolt.Tx) error {
		//create bucket users for users
		b, err := tx.CreateBucket([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		//insert admin user data
		err = b.Put([]byte(userData.Name+".Name"), []byte(userData.Name))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Password"), []byte(userData.Password))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Avatar"), []byte(userData.Avatar))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}
		err = b.Put([]byte(userData.Name+".Info"), []byte(userData.Info))
		if err != nil {
			return fmt.Errorf("Registration bolt: %s", err)
		}

		return nil
	})

	//insert a pair of secrets for admin
	err = db.Update(func(tx *bolt.Tx) error {
		//create the bucket for admin
		b, err := tx.CreateBucket([]byte(userData.Name))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		//populate with sample data
		err = b.Put([]byte("The Answer"), []byte("41"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		err = b.Put([]byte("Bank pin"), []byte("2121"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		return nil
	})

	this.Redirect("/", 200)

}
