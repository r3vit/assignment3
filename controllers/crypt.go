package controllers

import "golang.org/x/crypto/bcrypt"

//hashPassword returns the hashed password
func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

//comparePassword returns true if password (plain) and hash are the same
func comparePassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//encrypt returns an encrypted input based on key. (WIP)
func encrypt(plaintext []byte, key []byte) ([]byte, error) {
	//TODO: implement a real encrypt with user password
	return plaintext, nil
}

//decrypt returns the decrypted input based on key (WIP)
func decrypt(ciphertext []byte, key []byte) ([]byte, error) {
	//TODO: implement a real decrypt with user password
	return ciphertext, nil
}
